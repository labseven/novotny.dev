---
layout: list
title: "Music"
date: 2020-11-30
priority: 10
---

Some albums that I resonate with and recommend.

#### Format:

* Band Name (* if I have seen them live) - Some more info
    * Album name

## Rock

* Car Seat Headrest\* - these albums defined my college experience
    * Teens of Denial
    * Teens of Style
    * Twin Fantasy
* Japanese Breakfast\*
    * Psychopomp
    * BUMPER - pop songs 2020
    * Soft Sounds from Another Planet
* (Sandy) Alex G\*
    * Rocket
    * Beach Music
* The Magnetic Fields - When I played this in the library, the librarians were brought back to their years in college
    * 69 Love Songs
* Camp Cope
    * Camp Cope
* The Microphones
    * The Glow, Pt. 2 - Such raw sounds, the first time I heard this it blew my mind. A great album to drive home after a show to
    * Microphones in 2020
* The Beths\*
    * Future Me Hates Me
* Broken Social Scene\* - they're from canada
    * You Forgot It In People
    * Broken Social Scene
* Yo La Tengo
    * I Can Hear the Heart Beating As One
    * Summer Sun
* Brand New
    * The Devil And God Are Raging Inside Me
    * Science Fiction
* Black Midi\*
    * Schlagenheim
* The Velvet Underground
    * The Velvet Underground & Nico
* Lou Reed
    * Transformer


### Psychadelic

* Jay Som
    * Anak Ko
    * Turn Into
* King Gizzard & the Lizard Wizard* - They produce so many albums
    * Nonagon Infinity
    * K.G.
* Crumb
    * Locket
    * Crumb
* Melody's Echo Chamber
    * Melody's Echo Chamber
* Unknown Mortal Orchestra
 * Multi-Love
* Tame Impala
    * Lonerism
    * Currents
* Khruangbin
    * Mordechai



### Folkish

* Big Thief
    * Capacity
    * Masterpiece
    * U.F.O.F.
* Daughter
    * Not to Disappear
    * His Young Heart
* Sufjan Stevens
    * The Ascension
    * Carrie & Lowell
* Belle & Sebastian
    * If You’re Feeling Sinister
    * Tigermilk
* Elliott Smith
    * Either/Or
    * XO
* Mount Eerie
    * A Crow Looked at Me
    * Now Only
* The Mountain Goats* - perfect songs about imperfect people. I cried both times I saw them
    * The Sunset Tree
    * Goths
* Daniel Johnston
    * Hi How Are You - Weird, unfinished, a nervous breakdown

### Bedroom Rock

* Florist - Super quiet and intimate. For a quite night of introspection
    * Holdly
    * The Birds Outside Sang
    * Emily Alone
    * If Blue Could Be Happiness
* Soccer Mommy
    * Clean
* Snail Mail
    * Lush

### Post-rock
* Explosions in the Sky
    * The Earth Is Not a Cold Dead Place
* Godspeed You! Black Emperor
    * Luciferian Towers
    * Lift Your Skinny Fists Like Antennas to Heaven
* Mogwai
    * The Hawk Is Howling
    * Happy Songs For Happy People

### Math Rock
* toe
    * For Long Tomorrow
    * Hear You
* 3nd
    * World Tour

### Space/Dream Rock
* Duster
    * Stratosphere
    * Duster
* Beach House - My intro to dream pop
    * Depression Cherry
    * Bloom
    * 7
* Slowdive
    * Slowdive
* Real Estate
    * In Mind
    * Days

### Emo
* oso oso
    * basking in the glow
* American Football
    * American Football LP2 (2016 album)


## Punk

* Parquet Courts
    * Human Performance
    * Wide Awake!
* Jeff Rosenstock
    * WORRY.
    * POST-
    * Thanks, Sorry! - Amazing live album
* Hobo Johnson
    * The Fall of Hobo Johnson
* Diet Cig* - Really bouncy and fun
    * Swear I'm Good At This
* Gogol Bordello* - Didn't know them before I went to the show, and it was amazing. (Eugene Hütz was crowd surfing on a drum)
    * Gypsy Punks: Underdog World Strike
* The Front Bottoms* - Super produced but in a good way
    * Talon Of The Hawk
    * The Front Bottoms
* Pup* - There was so much energy pent up waiting for the first beat, the pit immediately erupted into a nonstop mosh.
* Mannequin Pussy\*
    * Patience
* Priests\*
    * Nothing Feels Natural


## Electronic

* Ryan Hemsworth
    * Guilt Trips - IDK why but this album captivates me
    * Alone For The First Time
* Blanck Mass - loud and harsh
    * Animated Violence Mild
    * Dumb Flesh
* Massive Attack
    * Mezzanine - Amazing album just dripping with sex
* Actress* - disappointed with his live show, but his album R.I.P. stands up
    * R.I.P.
* Burial
    * Untrue
* Skee Mask
    * Compro
* Max Cooper
    * One Hundred Billion Sparks - incredible music videos, watch [here](https://ohbs.maxcooper.net/)
* TOKiMONSTA
    * Creature Dreams
* Mathemagic
    * Mathemagic
* Futurecop!
    * Fairy Tales
* Boom Bip
    * Seed to Sun
* Nicolas Jaar
    * Sirens
* Phantogram
    * Three
* The Avalanches\*
    * Wildflower
* Monikino Kino
    * Prázdniny
* Thievery Corporation
    * The Richest Man In Babylon
* 100 gecs - these people are wild
    * 1000 gecs - also check out the remix album

### Experimental

* Botany
    * Lava Diviner (Truestory)
* Matmos
    * Ultimate Care II - Made only on a washing machine
    * Plastic Anniversary
* Xiu Xiu\*
    * FORGET
    * Always

## Ambient
* Emily A. Sprague - Lead singer of Florist
    * Water Memory
    * Mount Vision
* Stars of the Lid
    * The Tired Sounds of Stars of the Lid
    * And Their Refinement of the Decline
* Hiroshi Yoshimura
    * Music For Nine Post Cards
* Huerco S.
    * For Those Of You Who Have Never (And Also Those Who Have)
* William Basinski
    * The Disintegration Loops IV
    * A Shadow in Time

## Noise
* Merzbow / Sun Ra
    * Strange City

## Rap/Hip Hop

* ​R.A.P. Ferreira (Milo)
    * A Toothpaste Suburb
    * Who Told You to Think??!!?!?!?!
    * So The Flies Don't Come
* Open Mike Eagle
    * Anime, Trauma and Divorce
* Die Antwoord - Also see their movie Chappie
    * Ten$ion
    * Donker Mag


## Pop

* Dorian Electra - such an icon
    * Flamboyant
    * My Agenda
* TV Girl* - My first show during college.
    * French Exit
* Janelle Monáe
    * Dirty Computer - Watch the video album, it has incredible costumes and choreography.
* The xx
    * Coexist
* Julia Holter
    * Have You in My Wilderness
* Electric Light Orchestra (ELO) - Synthy and old, very lovely.
    * Time
    * Discovery
* Stereolab - french electronic pop
    * Emperor Tomato Ketchup
    * Dots And Loops
* Ida Maria
    * KATLA
* MIDI LIDI* - My favorite Czech band. They're a little whacky, and very fun.
    * Operace Kindigo
    * Give Masterpiece A Chance!
* Sidney Gish
    * No Dogs Allowed
* The Bird and the Bee
    * Ray Guns Are Not Just The Future
* Superorganism - so overproduced yet fun
    * Superorganism
* Sylvan Esso
    * Sylvan Esso
* Sleigh Bells
    * Treats
* Air*
    * Moon Safari
* Hundredth
    * Somewhere Nowhere

### Lo-fi
* Part Time
    * Late Night
* Porches.
    * Pool
    * Slow Dance in the Cosmos
* Grouper - "her older ambient stuff was better" -- a friend
    * Dragging a Dead Deer Up a Hill

## Jazz

* GoGo Penguin
    * v2.0
    * Man Made Object
* High Risk
    * High Risk

### Classical
* Ryuichi Sakamoto
    * async
* Ryan Teague
    * Coins & Crosses
