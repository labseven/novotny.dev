---
layout: list
title: "Videos"
date: 2020-01-06
priority: 100
---
Some videos that I resonate with and recommend.

### Art film

* Voyagers

### Music Videos

* [Arena](https://vimeo.com/259989412) (Páraic Mc Gloughlin): Stop motion from satelite imagery
* [Platonic]() : Incredible programmatic music video, using only footage from a single day.
