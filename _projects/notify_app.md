---
layout: project
title: "Coffee Notifications App"
date: 2019-04-12
published: true
banner: espresso.jpg
---
One pain point that I've been having at college has been organizing informal events with people outside of my immediate friends. I like to make coffee and host morning coffee drinking in my suite, but I don't have a good way to inform people that it is happening. The standard way to organize events at school is through an email list (or group chats), but nobody checks their email on a weekend morning.

![Espresso]({{ site.url }}/assets/img/p/espresso.jpg)

> One of my first shots

So I decided to make a lightweight webapp that would send push notifications to people's phones on coffee mornings. They can subscribe to new events and to day-before reminders. I can then sends everyone notifications, which has a much better response rate than email. Currently I have ~10% of the school subscribed.

[![Notification Website]({{ site.url }}/assets/coffee_notify_mockup.png)]({{ site.url }}/assets/coffee_notify_mockup.png)

> The current view of the notification feed

The first iteration was written in React and NodeJS. I rewrote it in Vue and Rails, which I found.

Look at it at [coffee.verynice.party](https://coffee.verynice.party).

I am rewriting the app in Phoenix to learn another framework, and to create a more maintainable codebase, allow users to set notifications per-event, and to make it easier for anyone to spin up their own page. This might spark a new era in informal events at Olin.
